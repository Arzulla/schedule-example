package com.example.schedulerexample;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class SchedulerExampleApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SchedulerExampleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("================= I AM HAPPY BIT*H  ====================");
    }
}
